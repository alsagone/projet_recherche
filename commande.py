
class Commande:
    def __init__(self, id_piece: str, quantite: int) -> None:
        self.id_piece = id_piece
        self.quantite = quantite

    def ajouter_quantite(self, quantite: int):
        self.quantite += quantite

    def to_dict(self) -> dict:
        return {
            "id_piece": self.id_piece,
            "quantite": self.quantite,
        }


def indice_commande(c: Commande, liste_commandes: list[Commande]) -> int:

    for indice_commande, commande in enumerate(liste_commandes):
        if c.id_piece == commande.id_piece:
            return indice_commande

    return -1


def indice_commande_id(piece_id: str, liste_commandes: list[Commande]) -> int:
    for indice_commande, commande in enumerate(liste_commandes):
        if piece_id == commande.id_piece:
            return indice_commande

    return -1
