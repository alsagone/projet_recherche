import graphe
import client

if __name__ == "__main__":
    d = graphe.sauvegarder_commandes(graphe.creer_dictionnaire_commandes())
    liste_clients = graphe.parse_fichier_commandes(d)

    dict_similarite = client.comparer_clients(liste_clients)
    print("Création de la matrice")
    m = graphe.json_to_matrix(dict_similarite)

    print("Création du graphe")
    g = graphe.construire_graphe(m)

    print("Louvain")
    graphe.louvain(g)

    print("Tarjan")
    graphe.tarjan(g)

    print("Girvan-Newman")
    graphe.girvan_newman(g)
