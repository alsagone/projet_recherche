import csv
import json
import time

import matplotlib.pyplot as plt
import networkx as nx
import networkx.algorithms.community as nx_comm
import numpy as np

import commande
from client import Client


def get_clients() -> dict:
    """Renvoie un dictionnaire liant les client_id et son index

    Retourne:
        dict: un dictionnaire
    """
    set_clients = set([])
    liste_clients = []
    dict_clients = {}

    with open("donnees/similarite.json") as f:
        data = json.load(f)

    for clients in data.values():
        for c in clients.keys():
            set_clients.add(c)

    liste_clients = list(set_clients)
    liste_clients.sort()

    for index, client_id in enumerate(liste_clients, start=0):
        dict_clients[client_id] = index

    return dict_clients


def creer_dictionnaire_commandes():
    dict_commandes = {}

    with open("donnees/commandes_small.csv") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)

        for row in csv_reader:
            id_piece, id_client, quantite = row[1], row[2], int(row[4])

            c = commande.Commande(id_piece, quantite)

            # Si le client existe déjà dans le dictionnaire
            if id_client in dict_commandes:

                # On vérifie si le client a déjà commandé la pièce
                indice_commande = commande.indice_commande(
                    c, dict_commandes[id_client])

                # Si ce n'est pas le cas, on ajoute simplement la commande à sa liste de commandes
                if indice_commande == -1:
                    dict_commandes[id_client].append(c)

                # Sinon, on met à jour la quantité
                else:
                    dict_commandes[id_client][indice_commande].ajouter_quantite(
                        c.quantite)

            # Si c'est un nouveau client, on l'ajoute simplement au dictionnaire
            else:
                dict_commandes[id_client] = [c]

    return dict_commandes


def sauvegarder_commandes(dict_commandes: dict):
    """Crée et enregistre un dictionnaire liant une pièce et la liste des clients l'ayant commandé
    """

    d = {}

    for id_client, liste_commandes in dict_commandes.items():
        d[id_client] = [c.to_dict() for c in liste_commandes]

    return d


def parse_fichier_commandes(data: dict) -> list[Client]:
    liste_clients: list[client.Client] = []

    for id_client, commandes in data.items():

        liste_commandes = [commande.Commande(
            com['id_piece'], com['quantite']) for com in commandes]
        client = Client(id_client, liste_commandes)
        liste_clients.append(client)

    return liste_clients


def get_json_adjacence():
    with open("donnees/similarite.json") as f:
        adj = json.load(f)

    return adj


def json_to_matrix(json_data: dict) -> np.ndarray:
    """D'après l'objet JSON renvoyé par la fonction json_adjacence(), construit une matrice d'adjacence client X client

    Args:
        json_data (dict): un dictionnaire (issu de la fonction json_adjacence())

    Returns:
        np.ndarray: une matrice d'adjacence
    """

    dict_clients = get_clients()
    nb_clients = len(dict_clients.keys())

    i = 0
    m = np.zeros((nb_clients, nb_clients))

    for client_id in json_data.keys():
        other_clients = json_data[client_id]

        indice_client_1 = dict_clients[client_id]

        for other_client_id in other_clients.keys():
            adjacence_value = json_data[client_id][other_client_id]

            indice_client_2 = dict_clients[other_client_id]

            m[indice_client_1, indice_client_2] = adjacence_value
            m[indice_client_2, indice_client_1] = adjacence_value

    valeur_moyenne = np.mean(m) // 10
    print(f"Valeur moyenne: {valeur_moyenne}")
    print(f"{np.count_nonzero(m <= valeur_moyenne)} arêtes remplissent cette condition")
    for i in range(len(m)):
        for j in range(len(m[0])):
            if (i != j) and (m[i, j] <= valeur_moyenne):
                m[i, j] = 1.

            else:
                m[i, j] = 0.

    return m


def construire_graphe(m: np.ndarray) -> nx.Graph:
    """Construit un graphe d'après la matrice d'adjacence passée en paramètre

    Argument:
        matrice_adjacence (np.ndarray): une matrice d'adjacence

    Retourne:
        nx.Graph: un graphe
    """
    g = nx.from_numpy_matrix(m, create_using=nx.Graph)
    g.remove_nodes_from(list(nx.isolates(g)))
    return g


def girvan_newman(graph: nx.Graph):
    """Applique l'algorithme de Girvan-Newman au graphe passé en paramètre

    Argument:
        graph (nx.Graph): un graphe
    """

    start = time.time()
    communities = nx_comm.girvan_newman(graph)
    node_groups = []

    for com in next(communities):
        node_groups.append(list(com))

    color_map = []
    for node in graph:
        if node in node_groups[0]:
            color_map.append("red")
        elif node in node_groups[1]:
            color_map.append("orange")

        else:
            color_map.append("blue")

    end = time.time()

    print(f"Temps d'exécution: {end - start} secondes")
    print(f"{len(node_groups)} communautés trouvées")

    plt.title("Girwan-Newman sur 1500 clients")
    nx.draw(graph, node_color=color_map, node_size=50)
    plt.show()
    return


def louvain(graph: nx.Graph):
    """Applique l'algorithme de Louvain au graphe passé en paramètre

    Argument:
        graph (nx.Graph): un graphe
    """
    start = time.time()
    communities = nx_comm.louvain_communities(graph, seed=123)
    node_groups = []
    for com in communities:
        node_groups.append(list(com))

    end = time.time()

    color_map = []
    for node in graph:
        if node in node_groups[0]:
            color_map.append("red")
        elif node in node_groups[1]:
            color_map.append("orange")

        else:
            color_map.append("blue")

    print(node_groups)
    print(f"{len(node_groups)} communautés trouvées")
    print(f"Temps d'exécution: {end - start} secondes")
    plt.title("Louvain sur 1500 clients")
    nx.draw(graph, node_color=color_map, node_size=50)
    plt.show()
    return


def tarjan(graph: nx.Graph):
    """Applique l'algorithme de Tarjan au graphe passé en paramètre

    Argument:
        graph (nx.Graph): un graphe
    """
    start = time.time()

    communities = nx.connected_components(graph)
    node_groups = []
    for com in communities:
        node_groups.append(list(com))

    end = time.time()
    print(f"Temps d'exécution: {end - start} secondes")

    color_map = []
    for node in graph:
        if node in node_groups[0]:
            color_map.append("red")

        else:
            color_map.append("blue")

    print(node_groups)
    print(f"{len(node_groups)} communautés trouvées")
    plt.title("Algorithme de Tarjan")
    nx.draw(graph, node_color=color_map, node_size=50)
    plt.show()
    return
