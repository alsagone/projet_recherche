from __future__ import annotations

import math

import numpy as np

from commande import Commande, indice_commande, indice_commande_id


class Client:
    def __init__(self, client_id: str, liste_commandes: list[Commande] = []) -> None:
        self.client_id = client_id
        self.liste_commandes = liste_commandes.copy()

    def __eq__(self, __o: Client) -> bool:
        return self.client_id == __o.client_id

    def ajouter_commande(self, c: Commande) -> None:
        index = indice_commande(c, self.liste_commandes)

        # Si la commande n'est pas dans la liste
        if index == -1:
            self.liste_commandes.append(c)

        # Sinon, on met à jour la quantité de la commande déjà existante
        else:
            self.liste_commandes[index].ajouter_quantite(c.quantite)

    def pieces_commandees(self, autre_client: Client) -> list[str]:
        """Renvoie la liste des pièces commandées par les deux clients

        Argument:
            autre_client (Client): Autre client

        Retourne:
            list[str]: la liste des ids des pièces
        """
        set_pieces = set(
            [com.id_piece for com in self.liste_commandes + autre_client.liste_commandes])
        return list(set_pieces)

    def distance_euclidienne(self, autre_client: Client) -> float:
        """Calcule la distance euclidienne avec un autre client

        Argument:
            autre_client (Client): un autre client

        Retourne:
            float: la distance euclidienne entre les deux clients
        """
        if self.__eq__(autre_client):
            return 0.

        total = 0.
        pieces = self.pieces_commandees(autre_client)

        for piece_id in pieces:
            i1 = indice_commande_id(piece_id, self.liste_commandes)
            q1 = 0 if i1 == -1 else self.liste_commandes[i1].quantite

            i2 = indice_commande_id(piece_id, autre_client.liste_commandes)
            q2 = 0 if i2 == -1 else autre_client.liste_commandes[i2].quantite

            total += math.pow((q1 - q2), 2)

        return math.sqrt(total)

    def similarite_cosinus(self, autre_client: Client) -> float:
        """Calcule la similarité cosinus avec un autre client

        Argument:
            autre_client (Client): un autre client

        Retourne:
            float: la similarité entre les deux clients
        """
        # On récupère d'abord la liste de toutes les pièces commandées par les DEUX clients
        pieces = self.pieces_commandees(autre_client)
        produit_vectoriel = 0

        # c1 et c2 sont les vecteurs des quantités de pièces commandées
        c1 = np.ndarray([])
        c2 = np.ndarray([])

        for piece_id in pieces:
            # On cherche si le client 1 a commandé la pièce
            i1 = indice_commande_id(piece_id, self.liste_commandes)

            # Si oui, on stocke la quantité dans q1 - sinon, on la met à zéro
            q1 = self.liste_commandes[i1].quantite if i1 >= 0 else 0
            c1 = np.append(c1, q1)

            # Pareil pour l'autre client
            i2 = indice_commande_id(piece_id, autre_client.liste_commandes)
            q2 = autre_client.liste_commandes[i2].quantite if i2 >= 0 else 0
            c2 = np.append(c2, q2)

            produit_vectoriel += (q1 * q2)

        produit_normes = np.dot(c1, c1) * np.dot(c2, c2)
        return produit_vectoriel / math.sqrt(produit_normes)


def comparer_clients(liste_clients: list[Client]) -> dict:
    """Calcule les distances euclidiennes entre tous les clients et renvoie un dictionnaire

    Argument:
        liste_clients (list[Client]): une liste de clients

    Retourne:
        dict: un dictionnaire
    """
    d = {}

    for client_1 in liste_clients:
        for client_2 in liste_clients:

            if client_1.client_id in d and client_2.client_id in d[client_1.client_id]:
                continue

            distance = client_1.distance_euclidienne(client_2)

            if client_1.client_id in d:
                d[client_1.client_id][client_2.client_id] = distance

            else:
                d[client_1.client_id] = {
                    client_2.client_id: distance
                }

            if client_2.client_id in d:
                d[client_2.client_id][client_1.client_id] = distance

            else:
                d[client_2.client_id] = {
                    client_1.client_id: distance
                }

    return d
