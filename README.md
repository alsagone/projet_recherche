# Recherche et partionnement d'une population en un graphe

Par Hakim Abdouroihamane - Imad Eddine Al Hannawi - Mohammed Azizi - Abdelhamid Benkada
M1 Informatique

### Dépendances

Ce programme nécessite au moins la version `3.7` de Python.
Pour installer toutes les dépendances nécessaires au lancement du programme, il suffit d'entrer la commande `python -m pip install -r requirements.txt`.

### Lancement

Le programme principal se trouve dans le fichier `main.py`.
Après avoir installé toutes les dépendances, il suffit de le lancer avec la commande `python main.py`.
